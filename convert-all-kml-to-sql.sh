#!/bin/sh

echo "" > import-all-kml.sql

cd input
for file in `ls *.kml`
do
    echo "Converting $file ..."
    f=`echo $file | cut -d. -f1`
    xsltproc -v -o ../sql/$f.sql ../kml2sql.xsl $file 2> ../log/$f.log
    sed -i 1d ../sql/$f.sql
    cat ../sql/$f.sql >> ../import-all-kml.sql
    echo "$file converted to sql into sql/$f.sql"
done
