# kml2sql4mobicoopplatform

KML to SQL converter for Mobicoop Platform territory

## Description
A shell script used for mass conversion through XSLT from a list of KML files into a SQL file.
The SQL file respect the template to create rows in the `territory` table of Mobicoop Platform.

Another shell script `convert-all-kml-to-sql4ref.sh` is provided to feed the territory reference table.

## Prerequisites
- Your KML files must respect the format `<Polygon><outerBoundaryIs><LinearRing><coordinates>` or `<MultiGeometry><Polygon><outerBoundaryIs><LinearRing><coordinates>` for the geometry of the territories.
- The `xsltproc` command must be installed in your system.

## How to run

### `convert-all-kml-to-sql.sh`
1. Copy your KML files into the `input` folder.
2. In your KML files, replace in the `SimpleData` attributes the name of the attribute you wish to use as name in the database by the attribute name `NAME4MOBICOOP` . For big files, command line is useful and you can use this example: `sed -i 's/"NOM_EPCI"/"NAME4MOBICOOP"/' epci.kml`.
3. Type the command: `sh convert-all-kml-to-sql.sh`.
4. Get the results of each input file in the `sql` folder.
5. Get the concatenate version of all SQL files into a single file `import-all-kml.sql` created at the root folder of this program.


### `convert-all-kml-to-sql4ref.sh`
1. Copy your KML files into the `input` folder.
2. In your KML files, replace in the `SimpleData` attributes the name of the attribute you wish to use as name in the database by the attribute name `NAME4MOBICOOP` . For big files, command line is useful and you can use this example: `sed -i 's/"NOM_EPCI"/"NAME4MOBICOOP"/' epci.kml`.
3. In your KML files, replace in the `SimpleData` attributes the name of the attribute you wish to use as name in the database by the attribute name `REF4MOBICOOP` . For big files, command line is useful and you can use this example: `sed -i 's/"CODE_EPCI"/"REF4MOBICOOP"/' epci.kml`.
4. If you want to add a prefix to each of the "REF4MOBICOOP" to ensure unicity, you can use a convenient command line like: `sed -i 's/"REF4MOBICOOP">\(.*\)</"REF4MOBICOOP">EPCI\1</' *depart*` .
5. Type the command: `sh convert-all-kml-to-sql4ref.sh`.
6. Get the results of each input file in the `sql` folder.
7. Get the concatenate version of all SQL files into a single file `import-all-kml.sql` created at the root folder of this program.

## Tips

### Tips
- If you have issues, take a look at what is outputted in the `log` folder.
- If you need to convert source files (like ESRI Shapefile .shp) to KML, you can use QGIS: load the Shapefile as new vector layer, and export it as KML file.
- If you want to simplify source files (reduce the number of points in the polygons), you can use the `Vector menu -> Geometry tools -> Simplify Geometries` command of QGIS and we use for IGN source data the value `0,0005` as simplification tolerance (use the default `Douglas-Peucker` algorithm).
- If the precision of your input files is high, you can reduce it to the advised 5 decimals standard that we use for territories in Mobicoop Platform as it is advised in the GeoJSON format. You can use this perl command for that in an example where your source file uses between 10 and 15 decimals: `perl -i -pe 's/([0-9]{1,2}\.[0-9]{10,15})/sprintf "%.05f", $1/eg' *.kml`.
- If you want to add a label to each of the "NAME4MOBICOOP", you can use a convenient command line like: `sed -i 's/"NAME4MOBICOOP">\(.*\)</"NAME4MOBICOOP">\1 (Département)</' *depart*` .

### Source data
- For France administrative boundaries, the best official data source is IGN **ADMIN EXPRESS** project available here: https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#admin-express

