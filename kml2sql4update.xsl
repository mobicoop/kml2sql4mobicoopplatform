<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2">
    <xsl:template match="/">
                    <xsl:for-each select="kml:kml/kml:Document/kml:Folder/kml:Placemark">
                        <xsl:variable name="name4mobicoop"><xsl:value-of select="kml:ExtendedData/kml:SchemaData/kml:SimpleData[@name='NAME4MOBICOOP']" /></xsl:variable>
                        <xsl:variable name="inseeDep"><xsl:value-of select="kml:ExtendedData/kml:SchemaData/kml:SimpleData[@name='INSEE_DEP']" /></xsl:variable>
                        <xsl:variable name="coordSingle"><xsl:value-of select="kml:Polygon/kml:outerBoundaryIs/kml:LinearRing/kml:coordinates"/></xsl:variable>
                        <xsl:variable name="multiGeometryCount">
                            <xsl:choose>
                                <xsl:when test="string-length($coordSingle) = 0"><xsl:for-each select="kml:MultiGeometry/kml:Polygon"><xsl:if test="position() = last()"><xsl:value-of select="last()" /></xsl:if></xsl:for-each></xsl:when>
                                <xsl:otherwise>1</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="mysqlCoord">
                            <xsl:choose>
                                <xsl:when test="string-length($coordSingle) = 0"><xsl:value-of select="translate(translate(translate(normalize-space(kml:MultiGeometry/kml:Polygon/kml:outerBoundaryIs/kml:LinearRing/kml:coordinates),',','§'),' ',','),'§',' ')"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="translate(translate(translate(normalize-space($coordSingle),',','§'),' ',','),'§',' ')" /></xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="string-length($coordSingle) = 0">

                                        UPDATE territory
                                        SET name = CONCAT(name, ' (', '<xsl:value-of select="$inseeDep" />', ')')
                                        WHERE id = (select min(id) from territory where name = "<xsl:value-of select="$name4mobicoop" />" and admin_level=4);

                                </xsl:when>
                                <xsl:otherwise>

                                        UPDATE territory
                                        SET name = CONCAT(name, ' (', '<xsl:value-of select="$inseeDep" />', ')')
                                        WHERE id = (select min(id) from territory where name = "<xsl:value-of select="$name4mobicoop" />" and admin_level=4);

                                </xsl:otherwise>
                            </xsl:choose>
                    </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
